OPENID_PROVIDERS = [
    { 'name': 'Google', 'url': 'https://www.google.com/accounts/o8/id' },
    { 'name': 'Yahoo', 'url': 'https://me.yahoo.com' },
    { 'name': 'AOL', 'url': 'http://openid.aol.com/<username>' },
    { 'name': 'Flickr', 'url': 'http://www.flickr.com/<username>' },
    { 'name': 'MyOpenID', 'url': 'https://www.myopenid.com' }]

class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'this-really-needs-to-be-changed'
    # basedir = os.path.abspath(os.path.dirname(__file__))
    SQLALCHEMY_DATABASE_URI = 'postgresql://raber:930114@localhost:5432/geotel'  # Подключаем базу postgres
    UPLOAD_FOLDER = 'app/static/uploads/'
    ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    DEBUG_TB_INTERCEPT_REDIRECTS = False


class TestingConfig(Config):
    TESTING = True


class MailSetting(Config):
    # email server
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'your-gmail-username'
    MAIL_PASSWORD = 'your-gmail-password'

    # administrator list
    ADMINS = ['your-gmail-username@gmail.com']
    # basedir = os.path.abspath(os.path.dirname(__file__))

    # SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
    # app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://Rider:930114@localhost:5432/geotel'
