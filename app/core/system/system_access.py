def log_template_renders(sender, template, context, **extra):
    sender.logger.debug('Обработан шаблон "%s" с контекстом %s',
                        template.name or 'строка шаблона',
                        context)
def log_exception(sender, exception, **extra):
    sender.logger.debug('Произошло исключение/ошибка: %s', exception)
