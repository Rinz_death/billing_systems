from flask import Flask
# from flask.ext.admin_panel.menu import MenuLink
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.mail import Mail
from flask_debugtoolbar import DebugToolbarExtension

from app.core.system.system_access import log_template_renders, log_exception
from flask import template_rendered, got_request_exception


class core:
    #Сигналы узнают состояние сервиса(а правда ли оно работает?оО)
    def signals(self, app):
        template_rendered.connect(log_template_renders, app)
        got_request_exception.connect(log_exception, app)
    # Модуль почты
    def module_email(self, app):
        self.signals(app)
        mail = Mail(app)  # Почта
        return mail
    # Модуль ORM для работы с БД
    def orm_db_module(self, app):
        self.signals(app)
        db = SQLAlchemy(app)
        return db
    # Дебаг модуль
    def debug_module(self, app):
        self.signals(app)
        DebugToolbarExtension(app)
        # return self.dtb
    # Подключаем Админку
    def adm_module(self):
        from app.modules.admin_panel.models import ModelAdmControl
        # Включаем адм панель
        ModelAdmControl()
    # Подключаем сторонние модули которые являются частью ядра системы
    def module(self,app):
        self.debug_module(app)
        self.adm_module()
        self.signals(app)
    # Инициализация сервиса/проекта
    def init_app(self, path):
        # path = os.path.join('.', os.path.dirname('/home/farsik/PycharmProjects/geotelekom/app/static/'), 'static/js/sijax/')
        app = Flask('app')
        app.config.from_object('config.DevelopmentConfig')
        app.config['SIJAX_STATIC_PATH'] = path
        app.config['SIJAX_JSON_URI'] = '/static/js/sijax/json2.js'
        return app

    # Удаляем символы с строки
    def remove_symvol(self, values):
        rev = str(values)
        rem = rev.replace(',', '')
        rem = rem.replace('[', '')
        rem = rem.replace(']', '')
        return rem
    #Обработчик списков на каждый чих, пожалуйста... не убивайте меня за этот гавно-код ::(
    def spisok(self,value,znachenie):
        value_new = []
        sec = 0
        if znachenie == 'company':
            for i in value:
                sec += 1
                values = '<p>%r - %r</p>' % (sec,i.company)
                value_new.append(values)
        elif znachenie == 'jobs_phone':
            for i in value:
                sec += 1
                values = '<p>%r - %r</p>' % (sec,i.jobs_phone)
                value_new.append(values)
        elif znachenie == 'language':
            for i in value:
                sec += 1
                values = '<p>%r - %r</p>' % (sec,i.language_name)
                value_new.append(values)
        elif znachenie == 'obrazovanie':
            for i in value:
                sec += 1
                values = '<p>%r - %r</p>' % (sec,i.forming_name)
                value_new.append(values)
        elif znachenie == 'mobile':
            for i in value:
                sec += 1
                values = '<p>%r - %r</p>' % (sec,i.mobile_phone)
                value_new.append(values)
        elif znachenie == 'home_phone':
            for i in value:
                sec += 1
                values = '<p>%r - %r</p>' % (sec,i.home_phone)
                value_new.append(values)
        elif znachenie == 'domen':
            for i in value:
                sec += 1
                values = '<p>%r - %r</p>' % (sec,i.domen_name)
                value_new.append(values)
        elif znachenie == 'balance':
            for i in value:
                sec += 1
                values = '<p>%r - %r</p>' % (sec,i.saldo)
                value_new.append(values)
        else:
            value_new = 0
        return value_new
    # Сохраняем логи по каждому пользоваелю
    def loging(self, log):
        pass

    # Фреймворк
    def security(self):
        pass

    # Шифрование пользовательских данных(свой алгоритм или поп)
    def sha_x(self):
        pass

    # Дешифратор(а нужен ли?)
    def d_sha_x(self):
        pass

    # Создает потоки и запускает все модули, при сбое уничтожает поток тем самым не дает системе упасть полностью
    def core_x(self):
        pass

    # Перезапускает поток
    def restart_potok(self):
        pass

    # Удаление потока
    def remove_potok(self):
        pass
