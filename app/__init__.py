import os

from app.core.system.core import core

# Храним путь к корню директории проекта
path = os.path.join('.', os.path.dirname(__file__), 'static/js/sijax/')
# Инициализируем объект ядра
cores = core()
# Запускаем приложение и храним доступ к нему ввиде абстракции(ссылки)
app = cores.init_app(path)
# Запускаем ORM и храним ссылку на него
db = cores.orm_db_module(app)
cores.module(app)
mail = cores.module_email(app)
from app import views

