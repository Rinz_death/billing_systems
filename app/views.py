# coding=utf-8
from flask import render_template, request, session, redirect, jsonify, flash, url_for, abort
from flask_babelex import Babel
from app.core.system.loger import Loging_save
# import re
from app.modules.user.models import Users
from app.modules.user.info_query import query_info
from app.core.system.core import core
from app import app
import os
from werkzeug.utils import secure_filename

# core()
babel = Babel(app)  # Перевод Адм панели


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']


# Тестовая страница, не трогать и не чего не менять в его моделе
@app.route('/error')
def error():
    abort(401)

@app.route('/test/')
def test():
    from blinker import Namespace
    my_signals = Namespace()
    model_saved = my_signals.signal('model-saved')
    print(model_saved)
    # user = Users()
    # tupk = user.url_user(nickname)
    # if tupk == None:
    #     return redirect(url_for("error"))
    return render_template("test/test.html", test=1)


# Узнаем все о пользователе, выполняется до любого запроса(гет или пост)
# @app.before_request
# def before_request():
#     # на какой странице пользователь и по каким перемещается
#     # page = request.headers['Referer']
#     # ip пользователя - нет проблем
#     obj = Loging_save()
#     ip_user = obj.ip_new()
#     print(str(ip_user))


#########AJAX#####################3

@app.route('/_search', methods=['GET', 'POST'])
def add_numbers():
    # Создаем объект класса с запросами
    obj1 = query_info()
    # Принимающее значение
    gets = str(request.values.get('a'))
    filters = str(request.values.get('b'))
    mymatches = int(request.values.get('s'))
    if filters != 'None':
        print(filters)
        if filters == "domen":
            search = obj1.search_domen(str(gets), matches=mymatches)
        elif filters == "user":
            search = obj1.search_user(str(gets), matches=mymatches)
        else:
            search = obj1.search(str(gets), filters=filters, matches=mymatches)
    else:
        # Уходим в метод поиска
        # filters = 0
        search = obj1.search(str(gets), filters=filters, matches=mymatches)
    if search:
        cores = core()
        srez = cores.remove_symvol(search)
        return jsonify({'result': str(srez)})


@app.route('/_hz', methods=['GET', 'POST'])
def text_peregruzit():
    # Создаем объект класса с запросами
    Succ = Users()
    cores = core()
    obj1 = query_info()
    vibor = str(request.values.get('value'))
    Success = Succ.sessions()
    final = obj1.text_peregruz(vibor=vibor, user=Success)
   # srez = cores.remove_symvol(final)
    return jsonify({'result': final})


##############Конец Ajax гавна##########################
@app.route('/')
@app.route('/main')
def add_numbers1():
    return render_template('main.html')


# Профиль пользователя
@app.route('/lk-<username>')
def UserPage(username):
    user = Users()
    tupk = user.url_user(username)
    if tupk == None:
        return redirect(url_for(401))
    if 'x_key' in session:
        Succ = Users()
        Success = Succ.sessions()
        return render_template("lk_user/profile-page.html", success=Success)
    else:
        return redirect("login.html")


# Действия в корзине пользователя
@app.route('/profile-user-card', methods=['GET'])
def UserCard():
    if 'x_key' in session:
        Succ = Users()
        Success = Succ.sessions()
        # print(Success.phone_nimbers)
        number_order = request.values.get('close_order')
        reasons = request.values.get('reson')
        if number_order:
            Succ.close_order(number_order, reasons)
        obj = query_info()
        korzina = obj.search_coockies()
        val = obj.car(korzina)
        return render_template("lk_user/profile-user-card.html", success=Success, uslugi=val)
    else:
        return redirect("/")


@app.route('/profile-user-card', methods=['POST'])
def UserCart0():
    if 'x_key' in session:
        Succ = Users()
        Success = Succ.sessions()
        chn_order = request.form['chanel_order']
        if chn_order:
            Succ.chanel_order(chn_order)
        return render_template('lk_user/profile-user-card.html', success=Success)
    else:
        return redirect("/")


@app.route('/order_by', methods=['GET'])
def order_by():
    if 'x_key' in session:
        Succ = Users()
        Success = Succ.sessions()
        obj = query_info()
        korzina = obj.search_coockies()
        if korzina:
            val = obj.car(korzina)
            Succ.order_add(val, str(Success.id))
    return redirect('profile-user-card')


# Регистрация пользователя
# Регистрация пользователя
@app.route('/register-ticket', methods=['POST'])
def reg_form():
    #   Succ = Users()
    #   Success = Succ.sessions()
    user = request.form['nameuser']
    phone = request.form['phone']
    email = request.form['email']
    error = None
    if user and phone and email:
        obj = Users()
        # Выбранные услуги
        obj1 = query_info()
        korzina = obj1.search_coockies()
        if korzina:
            val = obj1.car(korzina)
            obj.reg(phone, email, user, value=val)
        else:
            error = "У вас пустая корзина"
    else:
        error = "Заполните все поля!!!"
    return render_template("lk_user/register-ticket.html", error=error)


@app.route('/register-ticket')
def Ticket():
    obj = query_info()
    korzina = obj.search_coockies()
    val = obj.car(korzina)
    return render_template("lk_user/register-ticket.html", car=val)


@app.route('/profile-user-settings', methods=['POST'])
def UserSettingsModer():
    if 'x_key' in session:
        Succ = Users()
        Success = Succ.sessions()
        # Вкладка "Аватар и данные системы"
        obj = Users()
        all_value = obj.form_obr(request.form)

        # Получаем аватарку
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            Succ.add_avatar(filename)
        return render_template("lk_user/profile-user-settings.html", success=Success)
    else:
        return redirect("login")


@app.route('/profile-user-settings')
def UserSettings():
    if 'x_key' in session:
        Succ = Users()
        Success = Succ.sessions()
        return render_template("lk_user/profile-user-settings.html", success=Success)
    else:
        return redirect("login")


# Выход и уничтожение сессии
@app.route('/logout')
def logout():
    if session['x_key']:
        session.clear()
        return render_template("main.html")
    else:
        return render_template("main.html")


## Информационные страницы
@app.route('/abonentam-tor.html')
def abonentam_tor():
    return render_template("info/abonentam-tor.html")


@app.route('/abonentam-tor-00.html')
def abonentam_tor_00():
    return render_template("info/abonentam-tor-00.html")


@app.route('/abonentam-tor-01.html')
def abonentam_tor_01():
    return render_template("info/abonentam-tor-01.html")


@app.route('/abonentam-tor-02.html')
def abonentam_tor_02():
    return render_template("info/abonentam-tor-02.html")


@app.route('/abonentam-tor-03.html')
def abonentam_tor_03():
    return render_template("info/abonentam-tor-03.html")


@app.route('/fmc.html')
def fmc():
    return render_template("info/fmc.html")


@app.route('/help-service.html')
def help_service():
    return render_template("info/help-service.html")


@app.route('/home-internet.html')
def home_internet():
    return render_template("info/home-internet.html")


@app.route('/home-telephone.html')
def home_telephone():
    return render_template("info/home-telephone.html")


@app.route('/internet.html')
def internet():
    return render_template("info/internet.html")


@app.route('/login')
def login_start():
    if 'x_key' in session:
        return redirect('lk-'+session['x_key'])
    else:
        return render_template("info/login.html")


@app.route('/login', methods=['POST'])
def login_new():
    Login = request.form['Login']
    Password = request.form['password']
    error = None
    #Логин исключительно ЦИФРЫ
    if Login.isdigit():
        Succ = Users()
        Success = Succ.auth(login=str(Login), password=str(Password))
        if Success:
            user = str(session['x_key'])
            return redirect('lk-'+user)
            # return render_template("lk_user/profile-page.html", success=Success)  # Заменить шаблон на ЛК
        else:
            error = "Введен не верно логин или пароль"
            # return redirect('login')
    else:
        error = "Логин должен состоять из цифр"
        # return redirect('login')
    return render_template('info/login.html', error=error)
@app.route('/mobilnie-tarifi-beeline.html')
def mob_tarif():
    return render_template("info/mobilnie-tarifi-beeline.html")


@app.route('/number-tv.html')
def number_tv():
    return render_template("info/number-tv.html")


@app.route('/radio.html')
def radio():
    return render_template("info/radio.html")


@app.route('/support.html')
def support():
    return render_template("info/support.html")


@app.route('/telefonia.html')
def telephonia():
    return render_template("info/telefonia.html")


@app.route('/web-ats.html')
def web_ats():
    return render_template("info/web-ats.html")


@app.route('/web-office.html')
def web_office():
    return render_template("info/web-office.html")


@app.route('/web-phone.html')
def web_phone():
    return render_template("info/web-phone.html")


@app.route('/wifi-all.html')
def wifi_all():
    return render_template("info/wifi-all.html")


@app.route('/web-bbb.html')
def web_bbb():
    return render_template("info/work-bbb.html")


@app.route('/web-gsm.html')
def web_gsm():
    return render_template("info/work-gsm.html")


# Перевод Адм панели
@babel.localeselector
def get_locale():
    if request.args.get('lang'):
        session['lang'] = request.args.get('lang')
    return session.get('lang', 'ru')
