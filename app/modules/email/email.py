from flask.ext.mail import Message

from app import app, mail
from config import MailSetting


# Почта
class Email:
    user = None
    password = None
    email = None
    phone = None
    order = None

    def seconds(self):
        ADMINS = MailSetting.ADMINS
        msg = Message('test subject', sender=ADMINS[0], recipients=self.email)
        msg.body = '%r, $r, %r, %r' % (self.user, self.password, self.order, self.phone)
        msg.html = '<b>HTML</b> body'
        with app.app_context():
            mail.send(msg)
