from datetime import datetime

from app import db

class User(db.Model):
    __tablename__ = 'user'  # Для миграции имя таблицы которое необходимо создать

    id = db.Column(db.BIGINT, primary_key=True)
    # Другое
    unique_nomer = db.Column(db.BIGINT, unique=True, nullable=False)
    # Доступ к аккаунту
    password = db.Column(db.VARCHAR(120), unique=False, nullable=False)
    login = db.Column(db.BIGINT, unique=True, nullable=False)  # Логин является номером телефона пользователя
    email = db.Column(db.VARCHAR(120), unique=True, nullable=False)
    # Личная информация
    city = db.Column(db.VARCHAR(120))  # Город
    country = db.Column(db.VARCHAR(120))  # Страна
    oblast = db.Column(db.VARCHAR(120))  # Область
    naselenii_punkt = db.Column(db.VARCHAR(120))  # Населенный пункт
    address = db.Column(db.VARCHAR(120))  # Адресс
    fio = db.Column(db.String(64), unique=False, nullable=False)  # Фамилия, имя, отчество
    family = db.Column(db.VARCHAR(64), default='Женат/Замужем')
    language = db.relationship('Language', backref='Пользователь', lazy='dynamic')
    contacts = db.relationship('Contacts', backref='Пользователь', lazy='dynamic')
    forming = db.relationship('Forming', backref='Пользователь', lazy='dynamic')  # Образование
    phone = db.Column(db.BIGINT, unique=True, nullable=False)  # Номер заказанного телефона
    gender = db.Column(db.String(32), default='муж/жен')  # Пол
    data_birth = db.Column(db.TIMESTAMP,default=datetime.utcnow)  # Дата рождения
    # order = db.Column(db.BIGINT, unique=True)  # Номер заявки
    jobs = db.relationship('Jobs', backref='Пользователь', lazy='dynamic')  # Опыт работы
    # Заявка
    # Начало заявки
    data_start = db.Column(db.DateTime, nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)
    # Конец заявки
    data_end = db.Column(db.TIMESTAMP)
    etap = db.Column(db.VARCHAR(120), default=u'На рассмотрении')  # на каком этапе идет заявка
    orders = db.relationship('Order', backref='Пользователь', lazy='dynamic')


    # comment = db.Column(db.String(255), unique=False)
    ava = db.Column(db.String(255), default="default-avatar.jpg")  # Аватарка
    type_user = db.Column(db.String(64), default=u'Физ лицо')
    social = db.relationship('Social', backref='Пользователь', lazy='dynamic')
    blog = db.Column(db.String)  # Стена
    ## Далее gидут связки с другими таблицами
    # # Кто из сотрудников ведет данную заявку
    kto_vedet = db.Column(db.BIGINT, db.ForeignKey('user_jobs.id'))
    notes_text = db.relationship('notes', backref='Пользователь', lazy='dynamic')
    balance = db.relationship('Balance', backref='Пользователь', lazy='dynamic')
    domen = db.relationship('Domen', backref='Пользователь', lazy='dynamic')
    phone_my = db.relationship('Phone_number', backref='Пользователь', lazy='dynamic')
    companys = db.relationship('Company_Details', backref='Пользователь', lazy='dynamic')

    folowers = db.relationship('Folowers', backref='Пользователь', lazy='dynamic')
    # Required for administrative interface
    def __repr__(self):
        return '<li class="collection-item avatar"> ' \
               '<i class="material-icons circle">folder</i>' \
               '<span class="title"><strong>Номер Тора:</strong> %r</span>' \
               '<br />Описание пользователя' \
               '<br /><strong>ФИО:</strong>&nbsp;%r' \
               '<br /><strong>Тип пользователя:</strong>&nbsp;%r' \
               '<br /><strong>Город:</strong>&nbsp;%r' \
               '<br /><strong>Семейное положение:</strong>&nbsp;%r' \
               '<br /><strong>Дата рождения:</strong>&nbsp;%r' \
               '<br /><a href="!#">Отписаться</a>' \
               '</p>' \
               '<a href="#!" class="secondary-content btn #283593 indigo darken-3">Подписаться</a>' \
               '</li>' % (self.login, self.fio, self.type_user, self.city, self.family, self.data_birth)
        # def __repr__(self):
        #     # return '%r, %r, %r, %r, %r, %r' % (
        #     #     self.login, self.password, self.kto_vedet, self.data_start, self.fio, self.email)
        #     return '%r' % self.fio


# Опыт работы
class Jobs(db.Model):
    __tablename__ = 'jobs'
    id = db.Column(db.BIGINT, primary_key=True)
    company = db.Column(db.VARCHAR(256))
    position = db.Column(db.VARCHAR(128))
    work_from = db.Column(db.TIMESTAMP)
    work_to = db.Column(db.TIMESTAMP)
    user = db.Column(db.BIGINT, db.ForeignKey('user.id'))

    def __repr__(self):
        return '%r %r' % (self.company, self.position)


# Языки
class Language(db.Model):
    __tablename__ = 'language'
    id = db.Column(db.BIGINT, primary_key=True)
    language_name = db.Column(db.VARCHAR(128))
    user = db.Column(db.BIGINT, db.ForeignKey('user.id'))

    def __repr__(self):
        return '%r' % (self.language_name)


# Контакты
class Contacts(db.Model):
    __tablename__ = 'contacts'
    id = db.Column(db.BIGINT, primary_key=True)
    contacts_name = db.Column(db.String(120), default="Основные контакты")
    jobs_phone = db.Column(db.BIGINT)
    jobs_phone_name = db.Column(db.String(32), default='Рабочий телефон')  # Рабочий телефон
    home_phone = db.Column(db.BIGINT)
    home_phone_name = db.Column(db.String(32), default='Домашний телефон')  # Домашний телефон
    mobile_phone = db.Column(db.BIGINT)
    mobile_phone_name = db.Column(db.String(32), default='Мобильны телефон')  # Сотовый телефон
    dopemail = db.Column(db.VARCHAR(120), unique=True)
    user = db.Column(db.BIGINT, db.ForeignKey('user.id'))

    def __repr__(self):
        return '%r %r %r' % (self.jobs_phone, self.home_phone, self.mobile_phone)


# Образование
class Forming(db.Model):
    __tablename__ = 'forming'
    id = db.Column(db.BIGINT, primary_key=True)
    forming_name = db.Column(db.VARCHAR(128))
    forming_from = db.Column(db.TIMESTAMP)
    forming_to = db.Column(db.TIMESTAMP)
    user = db.Column(db.BIGINT, db.ForeignKey('user.id'))

    def __repr__(self):
        return '%r' % (self.forming_name)


# Соц сети
class Social(db.Model):
    __tablename__ = 'social'
    id = db.Column(db.BIGINT, primary_key=True)
    vk = db.Column(db.VARCHAR(256))
    social_name = db.Column(db.VARCHAR(256))
    odnoklassniki = db.Column(db.VARCHAR(256))
    moi_mir = db.Column(db.VARCHAR(256))
    facebook = db.Column(db.VARCHAR(256))
    google_plus = db.Column(db.VARCHAR(256))
    twitter = db.Column(db.VARCHAR(256))
    instagram = db.Column(db.VARCHAR(256))
    picasa = db.Column(db.VARCHAR(256))
    linkedin = db.Column(db.VARCHAR(256))
    user = db.Column(db.BIGINT, db.ForeignKey('user.id'))

    def __repr__(self):
        return '%r' % (self.vk)


# Баланс
class Balance(db.Model):
    __tablename__ = 'balance'
    id = db.Column(db.BIGINT, primary_key=True)
    # Подтягивается пользователь, физ или юр лицо, состояние заявки
    No_order = db.Column(db.BIGINT, unique=True, nullable=False)
    plus = db.Column(db.BIGINT)  # Приход
    minus = db.Column(db.BIGINT)  # Расход
    saldo = db.Column(db.BIGINT)  # Сальдо
    # data = db.Column(db.TIMESTAMP)  # Когда был поступлен платеж
    data_dohod = db.Column(db.DATE)
    time_dohod = db.Column(db.TIME)
    platezh_system = db.Column(db.String(128))
    comment = db.Column(db.String)
    # Связка с др таблицами
    user = db.Column(db.BIGINT, db.ForeignKey('user.id'))

    def __repr__(self):
        return '%r %r %r %r' % (self.saldo, self.plus, self.minus, self.data_dohod)

class Company_Details(db.Model):
    __tablename__ = 'business_details'
    id = db.Column(db.BIGINT, primary_key=True)
    company_logo = db.Column(db.String(255), default="default-avatar.jpg")
    short_business_name = db.Column(db.VARCHAR(128))
    full_business_name = db.Column(db.VARCHAR(256))
    legal_adress = db.Column(db.VARCHAR(1024))
    actual_adress = db.Column(db.VARCHAR(1024))
    INN = db.Column(db.BIGINT)
    KPP = db.Column(db.BIGINT)
    OGRN = db.Column(db.BIGINT)
    date_registration = db.Column(db.TIMESTAMP)
    registar = db.Column(db.VARCHAR(1024))
    kind_of_activity = db.Column(db.VARCHAR(128))
    bank = db.Column(db.VARCHAR(128))
    ks = db.Column(db.VARCHAR(256))
    higest_position = db.Column(db.VARCHAR(64))
    higest_position_fio = db.Column(db.VARCHAR(128))
    contact_phone = db.Column(db.BIGINT)
    company_site = db.Column(db.VARCHAR(128))
    company_email = db.Column(db.VARCHAR(128))
    user = db.Column(db.BIGINT, db.ForeignKey('user.id'))
    company_domens = db.relationship('Domen', backref=u'Домен', lazy='dynamic')
    company_phone_numbers = db.relationship('Phone_number', backref=u'Номер телефона', lazy='dynamic')

    def __repr__(self):
        return '%r' % (self.short_business_name)
# Заявки
class Order(db.Model):
    __tablename__ = 'orders'
    id = db.Column(db.BIGINT, primary_key=True)
    nomer_order = db.Column(db.BIGINT, unique=True, autoincrement=True, nullable=False)  # Номер заявки
    etap = db.Column(db.VARCHAR(64), default=u'На рассмотрении', nullable=False)
    type_order = db.Column(db.VARCHAR(64), nullable=False)  # Тип заявки(домен/номер телефона)
    otdel = db.Column(db.BIGINT, nullable=False)  # Какому отделу принадлежить заявка
    jobs_user = db.Column(db.BIGINT, nullable=False)  # Ответственный сотрудник
    data = db.Column(db.TIMESTAMP)  # Дата обработки заявки(с этапа на этап)
    data_open = db.Column(db.TIMESTAMP)
    data_closed = db.Column(db.TIMESTAMP)
    reson_closed = db.Column(db.VARCHAR(256))
    domens = db.relationship('Domen', backref=u'Домены', lazy='dynamic')
    phone_numbers = db.relationship('Phone_number', backref=u'Номер договора', lazy='dynamic')
    status_pay = db.Column(db.VARCHAR(64))  # Оплачено или нет
    ## Далее идут связки с другими таблицами
    user_id = db.Column(db.BIGINT, db.ForeignKey('user.id'))  # Пользователь за этой заявкой

    def __repr__(self):
        return '%r' % (self.nomer_order)


# New - Таблица с Доменами
class Domen(db.Model):
    __table__name = 'Domen'
    id = db.Column(db.BIGINT, primary_key=True)
    domen_name = db.Column(db.VARCHAR(128), unique=True, nullable=False)
    status = db.Column(db.VARCHAR(64), default=u'Свободен', nullable=False)
    order_id= db.Column(db.BIGINT, db.ForeignKey('orders.id'))
    user_id = db.Column(db.BIGINT, db.ForeignKey('user.id'))  # Пользователь за этой заявкой
    company_id= db.Column(db.BIGINT, db.ForeignKey('business_details.id'))


    def __repr__(self):
        return '<li class="collection-item avatar"> ' \
               '<i class="material-icons circle">folder</i>' \
               '<span class="title">%r</span>' \
               'Описание номера' \
               '<br /><strong>Статус:</strong>&nbsp;%r' \
               '<br /><a href="!#">Оформить/купить отдельно</a>' \
               '</p>' \
               '<a href="#!" class="secondary-content btn #283593 indigo darken-3">Отложить</a>' \
               '</li>' % (self.domen_name, self.status)


# Сообщения, переписки сотрудник - клиент
class Messagers(db.Model):
    __tablename__ = 'messager'
    id = db.Column(db.BIGINT, primary_key=True)
    order = db.Column(db.BIGINT, unique=True,
                      nullable=False)  # Номер заявки - подтягивать пользователя и ответственного
    data = db.Column(db.TIMESTAMP)
    message = db.Column(db.TEXT)
    jobs_user = db.relationship('Jobs_user', backref='Заметки', lazy='dynamic')  # Кто ответил на сообщение

    def __repr__(self):
        return '%r, %r' % (self.order, self.message)


# Сотрудники "компании", в зависимости от должности, свой ЛК - Не готово
class Jobs_user(db.Model):
    __tablename__ = 'user_jobs'
    id = db.Column(db.BIGINT, primary_key=True)
    login = db.Column(db.VARCHAR(120), unique=True, nullable=False)
    fio = db.Column(db.VARCHAR(120))
    password = db.Column(db.VARCHAR(256), nullable=False)
    email = db.Column(db.VARCHAR(120), unique=True, nullable=False)
    phone = db.Column(db.BIGINT, unique=True, nullable=False)
    role = db.Column(db.VARCHAR(255), default='1', nullable=False)  # Должность
    data = db.Column(db.TIMESTAMP)  # Когда был последний раз
    # Пользователи / заявки которые ведет сотрудник
    user = db.relationship('User', backref='Ответственный', lazy='dynamic')

    # Привязка к "сообщениям"
    messager = db.Column(db.BIGINT, db.ForeignKey('messager.id'))
    # Required for administrative interface
    def __repr__(self):
        return '%r' % self.fio


# Администраторы, полный контроль(Адм панель)
class Admin_user(db.Model):
    __tablename__ = 'user_admin'
    id = db.Column(db.BIGINT, primary_key=True)
    login = db.Column(db.VARCHAR(120), unique=True, nullable=False)
    email = db.Column(db.VARCHAR(120), unique=True, nullable=False)
    phone = db.Column(db.BIGINT, unique=True, nullable=False)

    def __repr__(self):
        return '<Admin %r>' % (self.login)


# Номера телефонов
class Phone_number(db.Model):
    __tablename__ = 'number'
    id = db.Column(db.BIGINT, primary_key=True)
    phone = db.Column(db.BIGINT, unique=True, nullable=False)
    instans_phone = db.Column(db.String, default=u"Свободен", nullable=False)
    type = db.Column(db.String, default=u"Обычный", nullable=False)
    type_operator = db.Column(db.String)
    data_reg = db.Column(db.TIMESTAMP)
    order_id = db.Column(db.BIGINT, db.ForeignKey('orders.id'))
    user_id = db.Column(db.BIGINT, db.ForeignKey('user.id'))  # Пользователь за этой заявкой
    company_id= db.Column(db.BIGINT, db.ForeignKey('business_details.id'))
    def ret(self):
        def __repr__(self):
            return self.id
    def __repr__(self):
        return '<li class="collection-item avatar"> ' \
               '<i class="material-icons circle">folder</i>' \
               '<span class="title">%r</span>' \
               '<p class="phonetype"> %r<br>' \
               'Описание номера' \
               '<br /><strong>Статус:</strong>&nbsp;%r' \
               '<br /><strong>Оператор:</strong>&nbsp;%r' \
               '</p>' \
               '<a href="#!" onclick="getCookie(%r)" class="secondary-content btn #283593 indigo darken-3">Отложить</a>' \
               '</li>' % (self.phone, self.type, self.instans_phone, self.type_operator, self.phone)


# Заметки
class notes(db.Model):
    __tablename__ = 'note'
    id = db.Column(db.BIGINT, primary_key=True)
    zagolovok = db.Column(db.VARCHAR(64), nullable=False)
    text = db.Column(db.String, nullable=False)
    user = db.Column(db.BIGINT, db.ForeignKey('user.id'))
    # # Новости
    # class news(db.Model):
    #     __tablename__ = 'news'
    #     id = db.Column(db.BIGINT, primary_key=True)
    #     name = db.Column(db.VARCHAR(64), nullable=False)
    #     ava = db.Column(db.VARCHAR(64), nullable=False)
    #     text = db.Column(db.String, default=u"Несколько слов", nullable=False)


# Новостные посты
class news_post(db.Model):
    __tablename__ = 'news'
    id = db.Column(db.BIGINT, primary_key=True)
    image = db.Column(db.VARCHAR(255))
    videos = db.Column(db.VARCHAR(255))
    Text = db.Column(db.String, nullable=False)


class Game(db.Model):
    __tablename__ = 'game'
    id = db.Column(db.BIGINT, primary_key=True)
    icon = db.Column(db.VARCHAR(255))
    url = db.Column(db.VARCHAR(255))
    Text = db.Column(db.String, nullable=False)


class Group(db.Model):
    __tablename__ = 'group'
    id = db.Column(db.BIGINT, primary_key=True)
    ava = db.Column(db.VARCHAR(255))
    name = db.Column(db.VARCHAR(255))
    opisanie = db.Column(db.String, nullable=False)


class Audio(db.Model):
    __tablename__ = 'audio'
    id = db.Column(db.BIGINT, primary_key=True)
    name = db.Column(db.VARCHAR(255))
    url = db.Column(db.VARCHAR(255))
    opisanie = db.Column(db.String, nullable=False)


class Video(db.Model):
    __tablename__ = 'video'
    id = db.Column(db.BIGINT, primary_key=True)
    name = db.Column(db.VARCHAR(255))
    url = db.Column(db.VARCHAR(255))
    opisanie = db.Column(db.String, nullable=False)


class Folowers(db.Model):
    __tablename__ = 'folowers'
    id = db.Column(db.BIGINT, primary_key=True)
    name = db.Column(db.VARCHAR(255))
    user = db.Column(db.BIGINT, db.ForeignKey('user.id'))


class Category(db.Model):
    __tablename__ = 'category_page'
    id = db.Column(db.BIGINT, primary_key=True)
    name = db.Column(db.VARCHAR(255))
    activate = db.Column(db.VARCHAR(24))
    data_activate = db.Column(db.TIMESTAMP)
    data_deactivate = db.Column(db.TIMESTAMP)
    data_remove = db.Column(db.TIMESTAMP)
    # page = db.Column(db.BIGINT, db.ForeignKey('page.id'))


class Page(db.Model):
    __tablename__ = 'page'
    id = db.Column(db.BIGINT, primary_key=True)
    info = db.Column(db.String)
    help = db.Column(db.String)  # Подсказака по расположению блоков
    activate = db.Column(db.VARCHAR(24))
    data_activate = db.Column(db.TIMESTAMP)
    data_deactivate = db.Column(db.TIMESTAMP)
    data_remove = db.Column(db.TIMESTAMP)
    # category = db.Column(db.BIGINT, db.ForeignKey('category_info.id'))