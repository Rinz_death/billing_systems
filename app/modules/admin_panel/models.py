from flask.ext.admin.menu import MenuLink
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

from app import app, db
from app.modules.admin_panel.admin_panel import Filter_user
from app.modules.db import db_models


class ModelAdmControl:
    def __init__(self):
        # Инициализируем Admin-панель
        self.admin = Admin(app, name=u'Панель управления', template_mode='bootstrap3')  # Создание Админ панели
        self.functional()

    # Включаем функционал
    def functional(self):
        self.models()
        self.filter()
        self.inoe()

    ## Фильтры
    def filter(self):
        self.admin.add_view(Filter_user(db_models.User, db.session, category=u'Пользователи', name=u"Клиенты"))

    # Добавления пунктов для Admin-панели
    def models(self):
        self.admin.add_view(ModelView(db_models.Jobs_user, db.session, category=u'Пользователи', name=u"Сотрудники"))
        self.admin.add_view(
            ModelView(db_models.Admin_user, db.session, category=u'Пользователи', name=u"Администраторы"))
        self.admin.add_view(ModelView(db_models.Phone_number, db.session, category=u'Услуги', name=u"Номера телефонов"))
        self.admin.add_view(ModelView(db_models.Domen, db.session, category=u'Услуги', name=u"Домены"))
        self.admin.add_view(ModelView(db_models.Messagers, db.session, category=u'CRM', name=u"История сообщении"))
        self.admin.add_view(ModelView(db_models.Balance, db.session, category=u'Пользователи', name=u"Баланс клиентов"))
        self.admin.add_view(ModelView(db_models.Order, db.session, category=u'CRM', name=u"Заявки"))
        self.admin.add_view(ModelView(db_models.notes, db.session, category=u'CRM', name=u"Заметки"))
        self.admin.add_view(ModelView(db_models.news_post, db.session, category=u'Соц.Сеть', name=u"Новости"))
        self.admin.add_view(ModelView(db_models.Game, db.session, category=u'Соц.Сеть', name=u"Игры"))
        self.admin.add_view(ModelView(db_models.Group, db.session, category=u'Соц.Сеть', name=u"Группы"))
        self.admin.add_view(ModelView(db_models.Audio, db.session, category=u'Соц.Сеть', name=u"Аудиозаписи"))
        self.admin.add_view(ModelView(db_models.Video, db.session, category=u'Соц.Сеть', name=u"Видеозаписи"))
        self.admin.add_view(ModelView(db_models.Folowers, db.session, category=u'Соц.Сеть', name=u"Фолловеры"))
        self.admin.add_view(ModelView(db_models.Category, db.session, category=u'CMS', name=u"Добавить категорию"))
        self.admin.add_view(ModelView(db_models.Page, db.session, category=u'CMS', name=u"Добавить страницу"))
        self.admin.add_view(ModelView(db_models.Contacts, db.session, category=u'Пользователи', name=u"Контакты"))
        self.admin.add_view(ModelView(db_models.Social, db.session, category=u'Пользователи', name=u"Соц сети"))
        self.admin.add_view(ModelView(db_models.Forming, db.session, category=u'Пользователи', name=u"Образование"))
        self.admin.add_view(ModelView(db_models.Language, db.session, category=u'Пользователи', name=u"Языки"))
        self.admin.add_view(ModelView(db_models.Jobs, db.session, category=u'Пользователи', name=u"Опыт работы"))
        self.admin.add_view(ModelView(db_models.Company_Details, db.session, category=u'Пользователи', name=u'Карточка предприятия'))

    # Другое
    def inoe(self):
        self.admin.add_link(MenuLink(name='Выход', url='/'))
