from flask.ext.admin.contrib import sqla
from flask.ext.admin.contrib.sqla.filters import FilterEqual

from app.modules.db.db_models import User


class Filter_user(sqla.ModelView):
    # Фильтры
    column_filters = [
        FilterEqual(User.phone, u'Номер телефона'),  # , options=(('1', 'Yes'),('0', 'No'))
        FilterEqual(User.fio, u'ФИО'),
        FilterEqual(User.email, u'Почта'),
        # FilterEqual(User.order, u'Номер заявки')
    ]


class MyUserAdmin(sqla.ModelView):
    def __init__(self, session, name, excluded=None):
        if excluded:
            self.excluded_list_columns = excluded

        super(MyUserAdmin, self).__init__(User, session, name=name)
