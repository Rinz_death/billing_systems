from flask import session

from app.core.system.core import core
from app.core.system.loger import Loging_save
from app.modules.db.db_models import User, Phone_number, Domen, Order, Messagers


class query_info:
    # Вычислить данного пользователя
    def online_user(self):
        User_online = User.query.filter_by(unique_nomer=session['x_key']).one()
        # print(User_online.balance.all())
        return User_online

    # Домены
    def domains(self):
        domains = Domen.query.all()
        return domains

    # Услуги/Заявки
    def services(self):
        order = Order.query.all()
        return order

    # Сообщения
    def message(self):
        mess = Messagers.query.all()
        return mess

    # Платежи
    def payment(self):
        user = self.online_user()
        # pay = Balance.query.filter_by(user=user.id)
        return user

    # Фильтры
    def filters_search(self, filters, phone):
        filter1 = []
        for i in phone:
            tempvalue = str(i.type_operator)
            print(tempvalue)
            if tempvalue == str(filters):
                filter1.append(i)
        return filter1

    # Поиск по доменам
    def search_domen(self, searches, matches):
        domenall = Domen.query.all()
        search = []
        sovpadenia = matches
        sovpadenia_tmp = 0
        if searches != 'None':
            for i in domenall:
                tmpvalue = str(i)
                tmpfind = tmpvalue.find(searches)
                if sovpadenia > sovpadenia_tmp:
                    sovpadenia_tmp = sovpadenia_tmp + 1
                    if (tmpfind != -1):
                        search.append(i)
        else:
            search.append(
                "Ошибка (не введен домен)! Введите пожалуйста в поисковой строке искомый домен или сочетание букв")
        return search

    # Поиск по пользователям
    def search_user(self, searches, matches):
        userall = User.query.all()
        search = []
        sovpadenia = matches
        sovpadenia_tmp = 0
        if searches != 'None':
            for i in userall:
                tmpvalue = str(i)
                tmpfind = tmpvalue.find(searches)
                if sovpadenia > sovpadenia_tmp:
                    sovpadenia_tmp = sovpadenia_tmp + 1
                    if (tmpfind != -1):
                        search.append(i)
        else:
            search.append(
                "Ошибка (не введен пользователь)! Введите пожалуйста в поисковой строке искомого пользователя или сочетание букв")
        return search

    def search_coockies(self):
        loging = Loging_save()
        korzina = loging.coockies()  # Корзина
        return korzina

    # Корзина
    def car(self, value):
        nomer = []
        phone = self.gophone()
        for i in phone:
            for x in value:
                tmpvalue = str(i)
                tmpfind = tmpvalue.find(x)
                if (tmpfind != -1):
                    nomer.append(i)
            print(nomer)
        return nomer

    # Поиск
    def search(self, searches, filters, matches):
        nomer = []
        phone = self.gophone()
        sovpadina = matches
        sovpadina_tmp = 0
        if filters != 'None':
            filters_itog = self.filters_search(filters, phone)
            phone = filters_itog
        if searches.isdigit():
            for i in phone:
                tmpvalue = str(i)
                tmpfind = tmpvalue.find(searches)
                if sovpadina > sovpadina_tmp:
                    sovpadina_tmp = sovpadina_tmp + 1
                    if (tmpfind != -1):
                        nomer.append(i)
        else:
            nomer.append("Ошибка (не введен номер)! Введите пожалуйста в поисковой строке искомый номер")
        # else:
        #             err = self.error(err = "Нет такого номера")
        # else:
        #     err = self.error(err = "Введите цифровое значение")
        return nomer

    # Еб вашу мать, пизда системе
    def error(self, err):
        return err

    # Номера телефонов
    def gophone(self):
        phoneall = Phone_number.query.all()
        return phoneall

    def finance(self):
        user = self.online_user()
        balance = user.balance
        balance_new = self.Core.spisok(balance, 'balance')
        return '<strong>Номер ТОР:</strong>&nbsp;%r<br />' % balance_new

    def lichnoe(self):
        user = self.online_user()
        city = user.city
        data_birth = user.data_birth
        family = user.family
        company = user.jobs
        company_new = self.Core.spisok(company, 'company')
        jobs_phone = user.contacts
        jobs_phone_new = self.Core.spisok(jobs_phone, 'jobs_phone')
        language = user.language
        language_new = self.Core.spisok(language, 'language')
        obrazovanie = user.forming
        obrazovanie_new = self.Core.spisok(obrazovanie, 'obrazovanie')
        return '<strong>Город:</strong>&nbsp;%r<br />' \
               '<strong>Дата рождения:</strong>&nbsp;%r<br />' \
               '<strong>Семейное положение:</strong>&nbsp;%r<br />' \
               '<strong>Организация:</strong>&nbsp;%r<br />' \
               '<strong>Рабочий телефон:</strong>&nbsp;%r<br />' \
               '<strong>Языки:</strong>&nbsp;%r<br />' \
               '<strong>Образование:</strong>&nbsp;%r<br />' % (
                   city, data_birth, family, company_new, jobs_phone_new, language_new, obrazovanie_new)

    def resource(self):
        user = self.online_user()
        tor = user.login
        mobile = user.contacts
        mobile_new = self.Core.spisok(mobile, 'mobile')
        # gor_phone = user.contacts.jobs_phone
        home_phone = user.contacts
        home_phone_new = self.Core.spisok(home_phone, 'home_phone')
        domen = user.domen
        domen_new = self.Core.spisok(domen, 'domen')

        return '<strong>Номер ТОР:</strong>&nbsp;%r<br />' \
               '<strong>Сотовый номер:</strong>&nbsp;%r<br />' \
               '<strong>Городской номер:</strong>&nbsp;%r<br />' \
               '<strong>Домены:</strong>&nbsp;%r<br />' % (tor, mobile_new, home_phone_new, domen_new)

    def text_peregruz(self, vibor, user):
        self.Core = core()
        if str(vibor) == 'Финансы':
            final = self.finance()
        elif str(vibor) == 'Ресурсы':
            final = self.resource()
        else:
            final = self.lichnoe()
        print(final)
        return final
