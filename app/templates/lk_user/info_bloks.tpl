<div class="row">
    <div id="short-info" class="col s12">
    <div class="row">
    <div class="col s12">
      <ul class="tabs">
        <li class="tab col s3"><a class="active"href="#test1">Личное</a></li>
        <li class="tab col s3"><a href="#test2">Ресурсы</a></li>
        <li class="tab col s3"><a href="#test4">Финансы</a></li>
      </ul>
    </div>
    <div id="test1" class="col s12">
            <strong>Дата рождения:</strong>&nbsp;{{ success.data_birth }}<br />
            <strong>Семейное положение:</strong>&nbsp;{{ success.gender }}<br />
            {% for i in success.jobs %}
                <strong>Организация:</strong>&nbsp;{{ i.company }}<br />
                <strong>Должность:</strong>&nbsp;{{ i.position }}<br />
            {% endfor %}

            {% for i in success.contacts %}
                <strong>Рабочий телефон:</strong>&nbsp;{{ i.jobs_phone}}<br />
            {% endfor %}

            {% for i in success.language %}
                <strong>Языки:</strong>&nbsp;{{ i.language_name }}<br />
            {% endfor %}

            {% for i in success.forming %}
                <strong>Образование:</strong>&nbsp;{{ i.forming_name }}<br />
            {% endfor %}
    </div>
    <div id="test2" class="col s12">
        Извините, сервис в разработке
    </div>
    <div id="test4" class="col s12">Извините, сервис в разработе</div>
  </div>
    </div>
    <div id="full-info" class="col s12">
        <div class="row">
        <div class="col s12">
          <ul class="tabs">
            <li class="tab col s3"><a class="active"href="#test11">Личное</a></li>
            <li class="tab col s3"><a href="#test21">Ресурсы</a></li>
            <li class="tab col s3"><a href="#test41">Финансы</a></li>
          </ul>
        </div>
        <div id="test11" class="col s12">
            <h5>Адрес</h5>
            <strong>Страна:</strong>&nbsp;{{ success.country }}<br />
            <strong>Область:</strong>&nbsp;{{ success.oblast }}<br />
            <strong>Населенный пункт:</strong>&nbsp;{{ success.city }}<br />
            <strong>Адрес:</strong>&nbsp;{{ success.address }}<br />
            <h5>Личная информация</h5>
            <strong>Дата рождения:</strong>&nbsp;{{ success.data_birth }}<br />
            <strong>Пол:</strong>&nbsp;{{ success.gender }}<br />
            <strong>Семейное положение:</strong>&nbsp;{{ success.family }}<br />

            {% for i in success.language %}
                <strong>Языки:</strong>&nbsp;{{ i.language_name }}<br />
            {% endfor %}

            {% for i in success.forming %}
                <strong>Образование:</strong>&nbsp;{{ i.forming_name }}<br />
            {% endfor %}

            <h5>Работа</h5>
            {% for i in success.jobs %}
                <strong>Организация:</strong>&nbsp;{{ i.company }}<br />
                <strong>Должность:</strong>&nbsp;{{ i.position }}<br />
            {% endfor %}

            <h5>Контакты</h5>
            {% for i in success.contacts %}
                <strong>Рабочий телефон:</strong>&nbsp;{{ i.jobs_phone}}<br />
                <strong>Домашний телефон:</strong>&nbsp;{{ i.home_phone }}<br />
                <strong>Мобильный телефон:</strong>&nbsp;+{{ i.mobile_phone }}<br />
            {% endfor %}

            <h5>Социальные сети</h5>
            {% for i in success.social %}
                <strong>ВКонтакте:</strong>&nbsp;<a href="http://{{ i.vk }}">{{ i.vk }}</a><br />
                <strong>Одноклассники:</strong>&nbsp;<a href="http://{{ i.odnoklassniki }}">{{ i.odnoklassniki }}</a><br />
                <strong>Мой мир:</strong>&nbsp;<a href="http://{{ i.moi_mir }}">{{ i.moi_mir }}</a><br />
                <strong>Facebook:</strong>&nbsp;<a href="http://{{ i.facebook }}">{{ i.facebook }}</a><br />
                <strong>Google+:</strong>&nbsp;<a href="http://{{ i.google_plus }}">{{ i.google_plus }}</a><br />
                <strong>Twitter:</strong>&nbsp;<a href="http://{{ i.twitter }}">{{ i.twitter }}</a><br />
                <strong>Instagram:</strong>&nbsp;<a href="http://{{ i.instagram }}">{{ i.instagram }}</a><br />
                <strong>Picasa:</strong>&nbsp;<a href="http://{{ i.picasa }}">{{ i.picasa }}</a><br />
            {% endfor %}
        </div>
        <div id="test21" class="col s12">Извините, сервис в разработке</div>
        <div id="test41" class="col s12">Извините, сервис в разработке</div>
        </div>
    </div>