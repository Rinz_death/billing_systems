<nav class="#424242 grey darken-3 white-text">
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          <li><a href="http://www.geotel.ru">Сайт "Геотелекома"</a></li>
          {% if session["x_key"] %}
            <li><a href="lk-{{ session["x_key"] }}">Личный кабинет</a></li>
            <li><a href="logout">Выход</a></li>
            {% else %}
                <li><a href="login">Вход</a></li>
                <li><a href="register-ticket">Регистрация/оформление заявки</a></li>
            {% endif %}
          <li><a href="help-service.html">Помощь</a></li>
        </ul>
        <ul id="slide-out" class="side-nav #424242 grey darken-3">
          <li><center><img src={{ url_for ('static', filename='img/defaults/tlogo_1.png') }}></center></li>
          <li><a href="mobilnie-tarifi-beeline.html" class="white-text">Мобильные тарифы ТОР (Beeline)</a></li>
          <li><a href="abonentam-tor.html" class="white-text">Абонентам ТОР</a></li>
          <li><a href="abonentam-tor-03.html" class="white-text">&nbsp;-&nbsp;Как стать абонентом ТОР?</a></li>
          <li><a href="abonentam-tor-00.html" class="white-text">&nbsp;-&nbsp;Что имеют абоненты ТОР?</a></li>
          <li><a href="abonentam-tor-01.html" class="white-text">&nbsp;-&nbsp;Офисы обслуживания ТОР</a></li>
          <li><a href="abonentam-tor-02.html" class="white-text">&nbsp;-&nbsp;Приложение</a></li>
          <li><a href="help-service.html" class="white-text">Справочная служба</a></li>
          <li><a href="internet.html" class="white-text">Интернет</a></li>
          <li><a href="telefonia.html" class="white-text">Телефония</a></li>
          <li><a href="home-internet.html" class="white-text">Домашний интернет</a></li>
          <li><a href="home-telephone.html" class="white-text">Домашний телефон</a></li>
          <li><a href="home-telephone.html" class="white-text">Домашний телефон</a></li>
          <li><a href="number-tv.html" class="white-text">Цифровое телевидение</a></li>
          <li><a href="radio.html" class="white-text">Радио</a></li>
          <li><a href="work-gsm.html" class="white-text">Корпоративная сотовая связь</a></li>
          <li><a href="web-office.html" class="white-text">Виртуальный офис</a></li>
          <li><a href="web-phone.html" class="white-text">Виртуальный телефон</a></li>
          <li><a href="web-ats.html" class="white-text">Виртуальная АТС</a></li>
          <li><a href="work-bbb.html" class="white-text">Видеоконференцсвязь</a></li>
          <li><a href="wifi-all.html" class="white-text">WiFi - везде</a></li>
          <li><a href="fmc.html" class="white-text">FMC</a></li>
          <li><a href="support.html" class="white-text">Техническая поддержка</a></li>
        </ul>
        <a href="#" data-activates="slide-out" class="button-collapse show-on-large"><i class="mdi-navigation-menu white-text"></i></a>
      </nav>
      <!-- Боковая панель навигации -->
    <script type="text/javascript">
          $('.button-collapse').sideNav({
          menuWidth: 340, // Default is 240
          edge: 'left', // Choose the horizontal origin
          closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
            }
          );
    </script>
