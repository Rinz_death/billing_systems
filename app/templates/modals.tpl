<!-- добавление социальные сети -->
<div id="modal-social" class="modal">
    <div class="modal-content">
        <h4>Добавить карточку социальных сетей</h4>
        <div class="row">
            <div class="input-field col s12">
                <input value="Введите название карточки социальных сетей" name="add_soc_name" id="add-contacts-name" type="text" class="validate">
                <label class="active" for="add-contacts-name">Наименование</label>
            </div>
            <div class="input-field col s12">
                <input value="Адрес страницы" name="add_vk" id="add-vk-web" type="text" class="validate">
                <label class="active" for="add-vk-web">ВКонтакте</label>
            </div>
            <div class="input-field col s12">
                <input value="Адрес страницы" name="add_ok" id="add-ok-web" type="text" class="validate">
                <label class="active" for="add-ok-web">Одноклассники</label>
            </div>
            <div class="input-field col s12">
                <input value="Адрес страницы" name="add_mm" id="add-my-world-web" type="text" class="validate">
                <label class="active" for="add-my-world-web">Мой мир</label>
            </div>
            <div class="input-field col s12">
                <input value="Адрес страницы" name="add_fb" id="add-facebook-web" type="text" class="validate">
                <label class="active" for="add-facebook-web">Facebook</label>
            </div>
            <div class="input-field col s12">
                <input value="Адрес страницы" name="add_gp" id="add-google-plus-web" type="text" class="validate">
                <label class="active" for="add-google-plus-web">Google+</label>
            </div>
            <div class="input-field col s12">
                <input value="Адрес страницы" name="add_tw" id="add-twitter-web" type="text" class="validate">
                <label class="active" for="add-twitter-web">Twitter</label>
            </div>
            <div class="input-field col s12">
                <input value="Адрес страницы" name="add_in" id="add-instagram-web" type="text" class="validate">
                <label class="active" for="add-instagram-web">Instagram</label>
            </div>
            <div class="input-field col s12">
                <input value="Адрес страницы" name="add_ps" id="add-picasa-web" type="text" class="validate">
                <label class="active" for="add-picasa-web">Picasa</label>
            </div>
            <div class="input-field col s12">
                <input value="Адрес страницы" name="add_ln" id="add-in-web" type="text" class="validate">
                <label class="active" for="add-in-web">LinkedIn</label>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn #424242 grey darken-3 white-text" type="submit" name="save">Сохранить изменения</button>
        <a class="modal-action modal-close waves-effect waves-green btn-flat">Отменить</a>
    </div>
</div>
<!-- Добавить образование -->
<div id="modal-education" class="modal">
    <div class="modal-content">
        <h4>Добавить оборазование</h4>
        <div class="row">
                <div class="input-field col s12">
                    <input value="Название учебного заведения" name="add-my-education" id="add-my-education" type="text" class="validate">
                    <label class="active" for="add-my-education">Наименование учебного заведения</label>
                </div>
                <div class="input-field col s6">
                    <input value="XXХХ-XX-XX" name="add-my-education-from" id="add-my-education-from" type="text" class="validate">
                    <label class="active" for="add-my-education-from">Когда поступил:</label>
                </div>
                <div class="input-field col s6">
                    <input value="XXхх-XX-XX" name="add-my-education-to" id="add-my-education-to" type="text" class="validate">
                    <label class="active" for="add-my-education-to">Когда закончил:</label>
                </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn #424242 grey darken-3 white-text" type="submit" name="save">Сохранить изменения</button>
        <a class="modal-action modal-close waves-effect waves-green btn-flat">Отменить</a>
    </div>
</div>
<!-- добалвение места работы -->
<div id="modal-work" class="modal">
    <div class="modal-content">
        <h4>Добавить место работы</h4>
        <div class="row">
                <div class="input-field col s12">
                    <input value="ООО 'Моя компания'" name="add-my-work" id="add-my-work" type="text" class="validate">
                    <label class="active" for="add-my-work">Наименование организации</label>
                </div>
                <div class="input-field col s12">
                    <input value="Должность" name="add-my-work-work" id="add-my-work-work" type="text" class="validate">
                    <label class="active" for="add-my-work-work">Наименование должности</label>
                </div>
                <div class="input-field col s6">
                    <input value="XXXX.XX.XX" name="add-my-work-from" id="add-my-work-from" type="text" class="validate">
                    <label class="active" for="add-my-work-from">Работа с:</label>
                </div>
                <div class="input-field col s6">
                    <input value="XXXX.XX.XX" name="add-my-work-to" id="add-my-work-to" type="text" class="validate">
                    <label class="active" for="add-my-work-to">Работа до:</label>
                </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn #424242 grey darken-3 white-text" type="submit" name="save">Сохранить изменения</button>
        <a class="modal-action modal-close waves-effect waves-green btn-flat">Отменить</a>
    </div>
</div>
<!-- добалвение карточки контактов -->
<div id="modal-contacts" class="modal">
    <div class="modal-content">
        <h4>Добавить место работы</h4>
        <div class="row">
            <form class="col s12">
                <div class="row">
                    <div class="input-field col s12">
                         <input name="new-name" value="Введите наименование" id="add-contacts-name" type="text" class="validate">
                         <label class="active" for="add-contacts-name">Наименование карточки контактов</label>
                     </div>
                     <div class="input-field col s6">
                         <input name="new-wpn" value="" id="add-work-phone-name" type="text" class="validate">
                         <label class="active" for="add-work-phone-name">Наименование рабочего номера</label>
                     </div>
                    <div class="input-field col s6">
                        <input name="new-wpnum" value="" type="text" class="validate">
                        <label class="active" for="add-work-phone-number">Номер рабочего телефона</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                         <input name="new-hpn" value="" id="add-work-name-name" type="text" class="validate">
                         <label class="active" for="add-home-phone-name">Наименование домашнего номера</label>
                     </div>
                    <div class="input-field col s6">
                        <input name="new-hpnum" value="" type="text" class="validate">
                        <label class="active" for="add-home-phone-number">Номер домашнего телефона</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                         <input name="new-mpn" value="Введите наименование" id="add-mobile-name-name" type="text" class="validate">
                         <label class="active" for="add-mobile-name-name-number">Наименование мобильного номера</label>
                     </div>
                    <div class="input-field col s6">
                        <input name="new-mpnum" value="" type="text" class="validate">
                        <label class="active" for="add-mobile-phone-number">Номер мобильного телефона</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                         <input name="new-m" value="" id="add-dopemail" type="text" class="validate">
                         <label class="active" for="add-dopemail">Дополнительная электронная почта</label>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal-footer">
           <div class="modal-footer">
            <button class="btn #424242 grey darken-3 white-text" type="submit" name="save">Сохранить изменения</button>
            <a class="modal-action modal-close waves-effect waves-green btn-flat">Отменить</a>
           </div>
    </div>
</div>
<!-- Добавление нового языка -->
<div id="modal-language" class="modal">
    <div class="modal-content">
        <h4>Добавить язык</h4>
        <div class="row">
                <div class="input-field col s12">
                    <input value="Клингонский" name="new-language" id="add-language" type="text" class="validate">
                    <label class="active" for="add-language">Наименование языка</label>
                </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn #424242 grey darken-3 white-text" type="submit" name="save">Сохранить изменения</button>
        <a class="modal-action modal-close waves-effect waves-green btn-flat">Отменить</a>
    </div>
</div>