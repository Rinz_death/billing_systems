    <footer class="page-footer #212121 grey darken-4">
          <div class="container">
            <div class="row">
              <div class="col l6 s124">
                <h5 class="white-text">Описание ТОР</h5>
                <p class="grey-text text-lighten-4">Текст описания системы ТОР.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Мы в социальных сетях</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Мы в "ВКонтакте"</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Мы в "Facebook"</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Мы в "Instagram"</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Мы в "Twitter"</a></li>
                  <li><a class="grey-text text-lighten-3" href="mailto:9292000001@mail.ru">Написать нам</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright #424242 grey darken-3">
            <div class="container">
            © 2014 Все права защищены "ГЕОТЕЛЕКОМ"
            <a class="grey-text text-lighten-4 right" href="http://www.it-work.pro">Разработанно "Миллениум. Интеграция"</a>
            </div>
          </div>
    </footer>
    </body>

  </html>